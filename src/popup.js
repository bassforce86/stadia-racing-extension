function load(){

  // Click handler for the Apply button
  document.getElementById('show_options').onclick = () => show();

  // Click handler for the links
  let links = document.querySelectorAll('a');
  for (const link in links) {
    if (links.hasOwnProperty(link)) {
      const a = links[link];
      a.onclick = function() {
        chrome.tabs.create({url: a.getAttribute('href')}); 
      }   
    }
  }

  // Get the extension version for display
  document.getElementById('version').innerText = chrome ? `v${chrome.app.getDetails().version}` : 'v00.00.00';
}

// Show All Monitors
function show() { 
  if (chrome.runtime.openOptionsPage) {
    chrome.runtime.openOptionsPage();
  } else {
    window.open(chrome.runtime.getURL('options.html'));
  } 
}

document.addEventListener('DOMContentLoaded', load);